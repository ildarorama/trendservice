package me.ildarorama.trend.service.component;

import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendPairEnum;

public interface TrendStoreWriteAccess {

    void store(TrendPairEnum pair, TrendItem item);
}
