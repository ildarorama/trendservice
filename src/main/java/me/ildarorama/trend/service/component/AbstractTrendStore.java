package me.ildarorama.trend.service.component;

import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.List;

public abstract class AbstractTrendStore implements TrendStoreReadAccess, TrendStoreWriteAccess {

    @Override
    abstract public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from, Long to);

    @Override
    abstract public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from);

    @Override
    abstract public void store(TrendPairEnum pair, TrendItem item);
}
