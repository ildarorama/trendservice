package me.ildarorama.trend.service.component;

import me.ildarorama.trend.service.model.QuoteItem;

public interface TrendConsumer {

    void process(QuoteItem quote);
}
