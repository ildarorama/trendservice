package me.ildarorama.trend.service.component;

import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendTypeEnum;

public class TrendReducer {
    private final TrendTypeEnum period;
    private long timestamp = -1;
    private double open;
    private double close;
    private double high;
    private double low;

    public TrendReducer(TrendTypeEnum period) {
        this.period = period;
    }

    public TrendItem reduce(long quoteTimestamp, double price) {
        var currentPeriod = quoteTimestamp / period.scale();
        currentPeriod = currentPeriod * period.scale();

        TrendItem item = null;
        if (currentPeriod != timestamp) {
            if (timestamp != -1) {
                item = new TrendItem(period, timestamp,
                        open, close,
                        high, low);
            }
            timestamp = currentPeriod;
            open = price;
            high = price;
            low = price;
        } else {
            high = Math.max(high, price);
            low = Math.min(low, price);
        }
        close = price;
        return item;
    }
}
