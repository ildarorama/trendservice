package me.ildarorama.trend.service.component.impl;

import me.ildarorama.trend.service.component.AbstractTrendStore;
import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class TrendItemListStoreImpl extends AbstractTrendStore {
    private final Map<TrendPairEnum, List<TrendItem>> m1Store =
            new EnumMap<>(TrendPairEnum.class);
    private final Map<TrendPairEnum, List<TrendItem>> h1Store =
            new EnumMap<>(TrendPairEnum.class);
    private final Map<TrendPairEnum, List<TrendItem>> d1Store =
            new EnumMap<>(TrendPairEnum.class);

    public TrendItemListStoreImpl() {
        for (TrendPairEnum value : TrendPairEnum.values()) {
            m1Store.put(value, new ArrayList<>());
            h1Store.put(value, new ArrayList<>());
            d1Store.put(value, new ArrayList<>());
        }
    }

    @Override
    public void store(TrendPairEnum pair, TrendItem item) {
        var store = switch (item.getPeriod()) {
            case M1 -> m1Store;
            case H1 -> h1Store;
            case D1 -> d1Store;
        };
        store.get(pair).add(item);
    }

    @Override
    public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from, Long to) {
        var store = switch (period) {
            case M1 -> m1Store;
            case H1 -> h1Store;
            case D1 -> d1Store;
        };
        return Collections.unmodifiableList(
                findSubList(store.get(pair), from, to)
        );
    }

    @Override
    public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from) {
        var store = switch (period) {
            case M1 -> m1Store;
            case H1 -> h1Store;
            case D1 -> d1Store;
        };
        return Collections.unmodifiableList(
                findSubList(store.get(pair), from, null)
        );
    }

    private List<TrendItem> findSubList(List<TrendItem> list, Long from, Long to) {
        int size = list.size();

        int fromIndex = findFromIndex(list, size, from);
        if (fromIndex == -1) {
            return List.of();
        }

        int toIndex = size - 1;
        if (to != null) {
            toIndex = findToIndex(list, size, to);
            if (toIndex == -1) {
                return List.of();
            }
        }

        return list.subList(fromIndex, toIndex + 1);
    }

    private int findFromIndex(List<TrendItem> list, int size, long to) {
        int low = 0;
        int high = size - 1;

        int toIndex = -1;
        while (low <= high) {
            int mid = (low + high) >>> 1;

            long cmp = list.get(mid).getTimestamp() - to;

            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp > 0) {
                high = mid - 1;
                toIndex = mid;
            } else {
                toIndex = mid;
                break;
            }
        }
        return toIndex;
    }

    private int findToIndex(List<TrendItem> list, int size, long from) {
        int low = 0;
        int high = size - 1;

        int toIndex = -1;
        while (low <= high) {
            int mid = (low + high) >>> 1;

            long cmp = list.get(mid).getTimestamp() - from;

            if (cmp < 0) {
                low = mid + 1;
                toIndex = mid;
            } else if (cmp > 0) {
                high = mid - 1;
            } else {
                toIndex = mid;
                break;
            }
        }
        return toIndex;
    }
}
