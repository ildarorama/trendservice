package me.ildarorama.trend.service.component.impl;

import me.ildarorama.trend.service.component.TrendConsumer;
import me.ildarorama.trend.service.component.TrendReducer;
import me.ildarorama.trend.service.component.TrendStoreWriteAccess;
import me.ildarorama.trend.service.model.QuoteItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.EnumMap;
import java.util.Map;

public class TrendConsumerImpl implements TrendConsumer {
    private final Map<TrendPairEnum, Map<TrendTypeEnum, TrendReducer>> stateMap = new EnumMap<>(TrendPairEnum.class);
    private final TrendStoreWriteAccess store;

    public TrendConsumerImpl(TrendStoreWriteAccess store) {
        for (TrendPairEnum value : TrendPairEnum.values()) {
            var periods = new EnumMap<TrendTypeEnum, TrendReducer>(TrendTypeEnum.class);

            for(TrendTypeEnum trend: TrendTypeEnum.values()) {
                periods.computeIfAbsent(trend, TrendReducer::new);
            }
            stateMap.put(value, periods);
        }
        this.store = store;
    }

    @Override
    public void process(QuoteItem quote) {
        var state = stateMap.get(quote.getPair());
        var pair = quote.getPair();

        for (TrendTypeEnum period : TrendTypeEnum.values()) {
            var item = state
                    .get(period)
                    .reduce(quote.getTimestamp(), quote.getPrice());
            if (item != null) {
                store.store(pair, item);
            }
        }
    }
}
