package me.ildarorama.trend.service.component;

import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.List;

public interface TrendStoreReadAccess {

    List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from, Long to);

    List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from);
}
