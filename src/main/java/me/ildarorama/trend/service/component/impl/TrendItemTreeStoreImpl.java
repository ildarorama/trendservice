package me.ildarorama.trend.service.component.impl;

import me.ildarorama.trend.service.component.AbstractTrendStore;
import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

public class TrendItemTreeStoreImpl extends AbstractTrendStore {
    private final Map<TrendPairEnum, ConcurrentSkipListMap<Long, TrendItem>> m1Store =
            new EnumMap<>(TrendPairEnum.class);
    private final Map<TrendPairEnum, ConcurrentSkipListMap<Long, TrendItem>> h1Store =
            new EnumMap<>(TrendPairEnum.class);
    private final Map<TrendPairEnum, ConcurrentSkipListMap<Long, TrendItem>> d1Store =
            new EnumMap<>(TrendPairEnum.class);

    public TrendItemTreeStoreImpl() {
        for (TrendPairEnum value : TrendPairEnum.values()) {
            m1Store.put(value, new ConcurrentSkipListMap<>());
            h1Store.put(value, new ConcurrentSkipListMap<>());
            d1Store.put(value, new ConcurrentSkipListMap<>());
        }
    }

    @Override
    public void store(TrendPairEnum pair, TrendItem item) {
        var store = switch (item.getPeriod()) {
            case M1 -> m1Store;
            case H1 -> h1Store;
            case D1 -> d1Store;
        };
        store.get(pair).put(item.getTimestamp(), item);
    }

    @Override
    public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from, Long to) {
        var store = switch (period) {
            case M1 -> m1Store;
            case H1 -> h1Store;
            case D1 -> d1Store;
        };
        var subMap = store.get(pair).subMap(from, true, to, true);
        return List.copyOf(subMap.values());
    }

    @Override
    public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from) {
        var store = switch (period) {
            case M1 -> m1Store;
            case H1 -> h1Store;
            case D1 -> d1Store;
        };
        var subMap = store.get(pair).tailMap(from);
        return List.copyOf(subMap.values());
    }
}
