package me.ildarorama.trend.service;

import me.ildarorama.trend.service.component.impl.TrendConsumerImpl;
import me.ildarorama.trend.service.component.impl.TrendItemListStoreImpl;
import me.ildarorama.trend.service.component.impl.TrendItemTreeStoreImpl;
import me.ildarorama.trend.service.model.QuoteItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;


public class TestPerformance {

    public static void main(String[] args) throws InterruptedException {
        long ts;
        var treeStore = new TrendItemTreeStoreImpl();
        var treeConsumer = new TrendConsumerImpl(treeStore);

        var listStore = new TrendItemListStoreImpl();
        var listConsumer = new TrendConsumerImpl(listStore);
        TrendService treeService = new TrendService(treeConsumer, treeStore);
        TrendService listService = new TrendService(listConsumer, listStore);

        treeService.start();
        listService.start();

        for (int i = 100; i < 1_000_000_000; i += 100) {
            treeService.consume(new QuoteItem(TrendPairEnum.EURUSD, 5, i));
            listService.consume(new QuoteItem(TrendPairEnum.EURUSD, 5, i));
        }
        treeService.gracefulShutdown();
        listService.gracefulShutdown();

        ts = System.nanoTime();
        var result = listService.request(TrendPairEnum.EURUSD, TrendTypeEnum.M1, 40_000_000L, 805_000_000L);
        ts = System.nanoTime() - ts;
        System.out.printf("List service result size %d, request time %,d ns%n", result.size(), ts);

        ts = System.nanoTime();
        result = treeService.request(TrendPairEnum.EURUSD, TrendTypeEnum.M1, 40_000_000L, 805_000_000L);
        ts = System.nanoTime() - ts;
        System.out.printf("Tree service result size %d, request time %,d ns%n", result.size(), ts);
    }
}
