package me.ildarorama.trend.service;

import me.ildarorama.trend.service.component.TrendConsumer;
import me.ildarorama.trend.service.component.TrendStoreReadAccess;
import me.ildarorama.trend.service.model.QuoteItem;
import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TrendService implements TrendStoreReadAccess {
    private final ArrayBlockingQueue<QuoteItem> queue = new ArrayBlockingQueue<>(1000);
    private final TrendConsumer trendConsumer;
    private final TrendStoreReadAccess store;
    private final Thread thread;

    private volatile boolean running = true;

    public TrendService(TrendConsumer trendConsumer, TrendStoreReadAccess store) {
        this.trendConsumer = trendConsumer;
        this.store = store;
        thread = new Thread(new QueueProcessor(), "Trend queue processor");
        thread.setDaemon(true);
    }

    public void consume(QuoteItem item) throws InterruptedException {
        if (running && validate(item)) {
            queue.put(item);
        }
    }

    public void start() {
        thread.start();
    }

    public void gracefulShutdown() throws InterruptedException {
        if (!running) {
            throw new IllegalStateException("Consumer service is not running");
        }
        running = false;
        thread.join();
    }

    private boolean validate(QuoteItem item) {
        return item != null && item.getPair() != null && item.getTimestamp() > 0;
    }

    @Override
    public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from, Long to) {
        return store.request(pair, period, from, to);
    }

    @Override
    public List<TrendItem> request(TrendPairEnum pair, TrendTypeEnum period, Long from) {
        return store.request(pair, period, from);
    }

    private class QueueProcessor implements Runnable {
        @Override
        public void run() {
            try {
                while (running || !queue.isEmpty()) {
                    var item = queue.poll(100L, TimeUnit.MILLISECONDS);
                    processItem(item);
                }
            } catch (InterruptedException e) {
                System.out.println("Trend service interrupted");
            }
        }

        void processItem(QuoteItem item) {
            if (item != null) {
                try {
                    trendConsumer.process(item);
                } catch (Exception e) {
                    System.out.println("Error processing " + item);
                }
            }
        }
    }
}
