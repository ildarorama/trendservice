package me.ildarorama.trend.service.model;

public enum TrendPairEnum {
    EURUSD, EURJPY;

    public static TrendPairEnum byId(int id) {
        return TrendPairEnum.values()[id];
    }
}
