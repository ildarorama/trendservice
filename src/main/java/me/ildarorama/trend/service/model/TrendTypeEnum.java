package me.ildarorama.trend.service.model;

public enum TrendTypeEnum {
    M1(60_000L), H1(3600_000), D1(86_400_000);

    private final long scale;

    TrendTypeEnum(long scale) {
        this.scale = scale;
    }

    public long scale() {
        return scale;
    }

    public static TrendTypeEnum byId(int id) {
        return TrendTypeEnum.values()[id];
    }
}
