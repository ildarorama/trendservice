package me.ildarorama.trend.service.model;

public class TrendItem {
    private final TrendTypeEnum period;
    private final Long timestamp;
    private final double open;
    private final double close;
    private final double high;
    private final double low;

    public TrendItem(TrendTypeEnum period, Long timestamp, double open, double close, double high, double low) {
        this.period = period;
        this.timestamp = timestamp;
        this.open = open;
        this.close = close;
        this.high = high;
        this.low = low;
    }

    public TrendTypeEnum getPeriod() {
        return period;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public double getOpen() {
        return open;
    }

    public double getClose() {
        return close;
    }

    public double getHigh() {
        return high;
    }

    public double getLow() {
        return low;
    }

    @Override
    public String toString() {
        return period + "/" + timestamp + "/" + open + "/" + close + "/" + high + "/" + low;
    }
}
