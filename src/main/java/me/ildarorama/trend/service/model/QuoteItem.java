package me.ildarorama.trend.service.model;

public class QuoteItem {
    private final TrendPairEnum pair;
    private final double price;
    private final long timestamp;

    public QuoteItem(TrendPairEnum pair, double price, long timestamp) {
        this.pair = pair;
        this.price = price;
        this.timestamp = timestamp;
    }

    public TrendPairEnum getPair() {
        return pair;
    }

    public double getPrice() {
        return price;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
