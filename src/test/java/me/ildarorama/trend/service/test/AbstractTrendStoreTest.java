package me.ildarorama.trend.service.test;

import me.ildarorama.trend.service.component.AbstractTrendStore;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.test.util.RandomTrendSupplier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static me.ildarorama.trend.service.model.TrendTypeEnum.M1;

public abstract class AbstractTrendStoreTest {
    protected AbstractTrendStore store;

    @Test
    public void testSave1000() {
        long initTimestamp = 1000;
        var itemGenerator = Stream.generate(new RandomTrendSupplier(M1, initTimestamp, 100));

        itemGenerator.limit(500).forEach(item -> store.store(TrendPairEnum.EURUSD, item));

        var savedBlock = store.request(TrendPairEnum.EURUSD, M1, initTimestamp);

        Assertions.assertEquals(savedBlock.size(), 500);
        Assertions.assertEquals(savedBlock.get(0).getTimestamp(), initTimestamp);
        Assertions.assertEquals(savedBlock.get(499).getTimestamp(), 1000 + 499 * 100);

        var beforeBlock = store.request(TrendPairEnum.EURUSD, M1, 0L, 999L);
        Assertions.assertTrue(beforeBlock.isEmpty());

        var afterBlock = store.request(TrendPairEnum.EURUSD, M1, 1000L + 500 * 100);
        Assertions.assertTrue(afterBlock.isEmpty());
    }

    @Test
    public void testSave1() {
        long initTimestamp = 1000;
        var itemGenerator = Stream.generate(new RandomTrendSupplier(M1, initTimestamp, 100));

        itemGenerator.limit(1).forEach(item -> store.store(TrendPairEnum.EURUSD, item));

        var savedBlock = store.request(TrendPairEnum.EURUSD, M1, initTimestamp);

        Assertions.assertEquals(savedBlock.size(), 1);
        Assertions.assertEquals(savedBlock.get(0).getTimestamp(), initTimestamp);

        var beforeBlock = store.request(TrendPairEnum.EURUSD, M1, 0L, 999L);
        Assertions.assertTrue(beforeBlock.isEmpty());

        var afterBlock = store.request(TrendPairEnum.EURUSD, M1, 1001L);
        Assertions.assertTrue(afterBlock.isEmpty());
    }

    @Test
    public void testSave2() {
        long initTimestamp = 1000;
        var itemGenerator = Stream.generate(new RandomTrendSupplier(M1, initTimestamp, 100));

        itemGenerator.limit(2).forEach(item -> store.store(TrendPairEnum.EURUSD, item));

        var savedBlock = store.request(TrendPairEnum.EURUSD, M1, initTimestamp);

        Assertions.assertEquals(savedBlock.size(), 2);
        Assertions.assertEquals(savedBlock.get(0).getTimestamp(), initTimestamp);
        Assertions.assertEquals(savedBlock.get(1).getTimestamp(), 1100);

        var beforeBlock = store.request(TrendPairEnum.EURUSD, M1, 0L, 999L);
        Assertions.assertTrue(beforeBlock.isEmpty());

        var afterBlock = store.request(TrendPairEnum.EURUSD, M1, initTimestamp + 500 * 100);
        Assertions.assertTrue(afterBlock.isEmpty());
    }

    @Test
    public void testEmpty() {
        var fromRange = store.request(TrendPairEnum.EURUSD, M1, 1000L);
        Assertions.assertTrue(fromRange.isEmpty());

        var toRange = store.request(TrendPairEnum.EURUSD, M1, 1000L, 1000_000_000L);
        Assertions.assertTrue(toRange.isEmpty());
    }
}
