package me.ildarorama.trend.service.test;

import me.ildarorama.trend.service.component.TrendReducer;
import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendTypeEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TrendReducerTest {

    @Test
    public void test1Minute() {
        TrendReducer reducer = new TrendReducer(TrendTypeEnum.M1);

        TrendItem item;

        item = reducer.reduce(0, 45);
        Assertions.assertNull(item);

        item = reducer.reduce(10_000, 23);
        Assertions.assertNull(item);

        item = reducer.reduce(30_000, 67);
        Assertions.assertNull(item);

        item = reducer.reduce(40_000, 5);
        Assertions.assertNull(item);

        item = reducer.reduce(59_999, 42);
        Assertions.assertNull(item);

        item = reducer.reduce(60_000, 85);
        Assertions.assertNotNull(item);

        Assertions.assertEquals(45, item.getOpen());
        Assertions.assertEquals(42, item.getClose());
        Assertions.assertEquals(5, item.getLow());
        Assertions.assertEquals(67, item.getHigh());
    }

    @Test
    public void testHalfRange() {
        TrendReducer reducer = new TrendReducer(TrendTypeEnum.M1);

        TrendItem item;

        item = reducer.reduce(45_000, 45);
        Assertions.assertNull(item);

        item = reducer.reduce(48_000, 23);
        Assertions.assertNull(item);

        item = reducer.reduce(52_000, 67);
        Assertions.assertNull(item);

        item = reducer.reduce(55_000, 5);
        Assertions.assertNull(item);

        item = reducer.reduce(59_999, 42);
        Assertions.assertNull(item);

        item = reducer.reduce(60_000, 85);
        Assertions.assertNotNull(item);

        Assertions.assertEquals(45, item.getOpen());
        Assertions.assertEquals(42, item.getClose());
        Assertions.assertEquals(5, item.getLow());
        Assertions.assertEquals(67, item.getHigh());
    }
}
