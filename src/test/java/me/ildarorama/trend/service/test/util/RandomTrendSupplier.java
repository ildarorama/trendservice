package me.ildarorama.trend.service.test.util;

import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.Random;
import java.util.function.Supplier;

public class RandomTrendSupplier implements Supplier<TrendItem> {
    private final Random rnd = new Random();
    private final TrendTypeEnum period;
    private final long timestampStep;
    private long startTimestamp;

    public RandomTrendSupplier(TrendTypeEnum period, long startTimestamp, long timestampStep) {
        this.period = period;
        this.startTimestamp = startTimestamp;
        this.timestampStep = timestampStep;
    }

    @Override
    public TrendItem get() {
        TrendTypeEnum itemPeriod = period;
        if (itemPeriod == null) {
            itemPeriod =  TrendTypeEnum.byId(rnd.nextInt(TrendTypeEnum.values().length));
        }
        long timestamp = startTimestamp;
        startTimestamp += timestampStep;
        return new TrendItem(itemPeriod, timestamp, 0, 0, 0, 0);
    }
}
