package me.ildarorama.trend.service.test;

import me.ildarorama.trend.service.component.TrendConsumer;
import me.ildarorama.trend.service.component.TrendStoreWriteAccess;
import me.ildarorama.trend.service.component.impl.TrendConsumerImpl;
import me.ildarorama.trend.service.model.TrendItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;
import me.ildarorama.trend.service.test.util.RandomQuoteSupplier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.function.Predicate;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class TrendConsumerTest {
    @Mock
    private TrendStoreWriteAccess store;
    @Captor
    private ArgumentCaptor<TrendItem> itemCaptor;
    @Spy
    private TrendConsumer consumer;

    @BeforeEach
    public void setUp() {
        consumer = new TrendConsumerImpl(store);
    }

    @Test
    public void testBeforeMinute() {
        long initTimestamp = 0;
        var itemGenerator = Stream.generate(new RandomQuoteSupplier(TrendPairEnum.EURUSD, initTimestamp, 1000));

        itemGenerator.limit(61).forEach(consumer::process);

        Mockito.verify(store, Mockito.times(1))
                .store(eq(TrendPairEnum.EURUSD), itemCaptor.capture());

        var hasNonM1 = itemCaptor.getAllValues()
                .stream()
                .map(TrendItem::getPeriod)
                .anyMatch(Predicate.not(TrendTypeEnum.M1::equals));

        Assertions.assertFalse(hasNonM1);
    }

    @Test
    public void testOneDay() {
        long initTimestamp = 0;
        int itemCount = 24 * 60 * 60 + 1;
        var itemGenerator = Stream.generate(new RandomQuoteSupplier(TrendPairEnum.EURUSD, initTimestamp, 1000));

        itemGenerator.limit(itemCount).forEach(consumer::process);

        int expectedInvocation = 24 * 60 + 24 + 1;

        Mockito.verify(store, Mockito.times(expectedInvocation))
                .store(eq(TrendPairEnum.EURUSD), itemCaptor.capture());

        long minutes = itemCaptor.getAllValues().stream().map(TrendItem::getPeriod)
                .filter(TrendTypeEnum.M1::equals)
                .count();
        Assertions.assertEquals(24 * 60, minutes);

        long hours = itemCaptor.getAllValues().stream().map(TrendItem::getPeriod)
                .filter(TrendTypeEnum.H1::equals)
                .count();
        Assertions.assertEquals(24, hours);

        long days = itemCaptor.getAllValues().stream().map(TrendItem::getPeriod)
                .filter(TrendTypeEnum.D1::equals)
                .count();
        Assertions.assertEquals(1, days);
    }
}
