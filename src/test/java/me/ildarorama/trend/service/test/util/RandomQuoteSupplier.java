package me.ildarorama.trend.service.test.util;

import me.ildarorama.trend.service.model.QuoteItem;
import me.ildarorama.trend.service.model.TrendPairEnum;
import me.ildarorama.trend.service.model.TrendTypeEnum;

import java.util.Random;
import java.util.function.Supplier;

public class RandomQuoteSupplier implements Supplier<QuoteItem> {
    private final Random rnd = new Random();
    private final TrendPairEnum pair;
    private final long timestampStep;
    private final Double price;
    private long startTimestamp;

    public RandomQuoteSupplier(TrendPairEnum pair, long startTimestamp, long timestampStep, Double price) {
        this.pair = pair;
        this.startTimestamp = startTimestamp;
        this.timestampStep = timestampStep;
        this.price = price;
    }

    public RandomQuoteSupplier(TrendPairEnum pair, long startTimestamp, long timestampStep) {
        this(pair, startTimestamp, timestampStep, null);
    }

    @Override
    public QuoteItem get() {
        TrendPairEnum itemPair = pair;
        if (itemPair == null) {
            itemPair =  TrendPairEnum.byId(rnd.nextInt(TrendTypeEnum.values().length));
        }
        double itemPrice;
        if (price != null) {
            itemPrice = price;
        } else {
            itemPrice = rnd.nextDouble(100);
        }
        long timestamp = startTimestamp;
        startTimestamp += timestampStep;
        return new QuoteItem(itemPair, itemPrice, timestamp);
    }
}
